<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Technique;
use App\Entity\Painting;
use App\Form\PaintingType;
use App\Repository\CommentRepository;
use App\Repository\PaintingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Faker;

class AdminController extends AbstractController
{
    #[Route('/admin', name: 'admin')]
    public function admin ( PaintingRepository $repository ): Response
    {
        $tableau = $repository->findBy(

            [],['Categorie' => 'ASC']);
        return $this->render('admin/admin.html.twig', [
            'admin' => $tableau,
        ]);
    }

    #[Route('/admin/new', name: 'new')]
    public function new ( Request $request, EntityManagerInterface $manager )
    {
        //   $faker = Faker\Factory::create();
        $paint = new Painting();
        $form = $this->createForm(PaintingType::class, $paint);
        $form->handleRequest($request);//recupere les donees du formulaire
        if ( $form->isSubmitted() && $form->isValid() ) {
            //  $paint ->setImage('default.png')
            $paint->createSlug();
            $paint->setRegisterAt(new \DateTimeImmutable("now"));
            $manager->persist($paint);
            $manager->flush();
            $this->addFlash('notice', 'votre action c\'est bien deroulée');
            return $this->redirectToRoute('admin');
        }

        return $this->renderForm('admin/new.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/admin/delete/{id}', name: 'delete')]
    public function delete ( Painting $tableau,  CommentRepository $repository,EntityManagerInterface $manager )
    {
        $comment = $repository->findBy(['relation' => $tableau], ['createdAt' => 'DESC']);
        $count= count($comment);
        if ($count<1){
        $manager->remove($tableau);
        $manager->flush();
        $this->addFlash('notice', 'votre action c\'est bien deroulée');
        return $this->redirectToRoute('admin');
    }else  $this->addFlash('notice', 'Vous ne pouvez pas effcer ce tableau, il existe des commentaires'); return
        $this->redirectToRoute('admin'); }

    #[Route('/admin/edit/{id}', name: 'edit')]
    public function edit ( Painting $tableau, EntityManagerInterface $manager, Request $request ): Response
    {
        $form = $this->createForm(PaintingType::class, $tableau);
        $form->handleRequest($request);
        if ( $form->isSubmitted() && $form->isValid() ) {
            $tableau->createSlug();
            $manager->persist($tableau);

            $manager->flush();
            $this->addFlash('notice', 'votre action c\'est bien deroulée');
            return $this->redirectToRoute('admin');
        }
        return $this->renderForm('/admin/edit.html.twig', [
            'form' => $form,
        ]);
    }
}
