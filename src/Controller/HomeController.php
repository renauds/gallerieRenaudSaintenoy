<?php

namespace App\Controller;


use App\Entity\Comment;
use App\Entity\Painting;
use App\Form\CommentType;
use App\Repository\PaintingRepository;
use App\Repository\CommentRepository;
use App\Repository\CategorieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Faker;

class HomeController extends
    AbstractController
{
    #[Route('/', name: 'home')]
    public function home ( PaintingRepository $repository, CategorieRepository $categorieRepository,
                           PaginatorInterface $paginator,
Request $request ): Response
    {
        $tableau = $repository->findBy([], ['Categorie' => 'ASC']);
        $categorie = $categorieRepository->findAll();
        $pagination = $paginator->paginate(
            $tableau,
            $request->query->getInt('page', 1),
            8
        );
        return $this->render('home/home.html.twig', [
                'home' => $tableau,
                'home' => $pagination,
                'categories'=>$categorie
            ]
        );
    }

    #[Route('home/{slug}', name: 'riri')]
    public function detail (
        Painting               $tableau,
        CommentRepository      $repository,
        Request                $request,
        EntityManagerInterface $manager
    ): Response {
        $faker = Faker\Factory::create('fr_FR');
        $comment = $repository->findBy(['relation' => $tableau], ['createdAt' => 'DESC']);
        $commentaire = new Comment();
        $form = $this->createForm(CommentType::class, $commentaire);
        $form->handleRequest($request);//recupere les donees du formulaire
        if ( $form->isSubmitted() && $form->isValid() ) {
            //  $paint ->setImage('default.png')

            $commentaire->setCreatedAt(new \DateTimeImmutable("now"));
            $commentaire->setRelation($tableau);
            $commentaire->setIsPublished($faker->boolean(90));
            $manager->persist($commentaire);
            $manager->flush();
            $this->addFlash('notice', 'votre commentaire a bien ete ajouté');
            return $this->redirectToRoute('home');
        }
        return $this->render('home/detail.html.twig', [
            'riri' => $tableau,
            'comments' => $comment,
            'form' => $form->createView(),
        ]);
    }
}
