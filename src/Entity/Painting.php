<?php

namespace App\Entity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\PaintingRepository;
use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
#[ORM\Entity(repositoryClass: PaintingRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[UniqueEntity(
    fields: ['Titre'], message: 'cette donnee existe deja',)]
#[Vich\Uploadable]

class Painting
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\Length(
        min: 2,
        max: 255,
        minMessage: 'Votre titre doit contenir au moins {{ limit }} characteres',
        maxMessage: 'Votre titre ne peux pas depasser {{ limit }} characteres',
    )]
    private ?string $Titre = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\Length(
        min: 10,

        minMessage: 'Votre contenu doit avoir au moins {{ limit }} characteres',

    )]
    private ?string $Description = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $CreatedAt = null;

    #[ORM\Column]
    #[Assert\Length(
        min: 2,

        minMessage: 'Votre longueur doit avoir au moins {{ limit }} characteres',

    )]
    private ?int $Hauteur = null;

    #[ORM\Column]
    #[Assert\Length(
        min: 2,

        minMessage: 'Votre largeur doit avoir au moins {{ limit }} characteres',

    )]
    private ?int $Largeur = null;

   // #[ORM\Column(length: 255)]
   // private ?string $Image = null;

    #[ORM\ManyToOne(inversedBy: 'paintings')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Technique $Technique = null;

    #[ORM\ManyToOne(inversedBy: 'paintings')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Categorie $Categorie = null;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    // NOTE: This is not a mapped field of entity metadata, just a simple property.
    #[Vich\UploadableField(mapping: 'image', fileNameProperty: 'Image')]
    private ?File $imageFile = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $Image= null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $RegisterAt = null;

    #[ORM\OneToMany(mappedBy: 'relation', targetEntity: Comment::class)]
    private Collection $comments;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->Titre;
    }

    public function setTitre(string $Titre): self
    {
        $this->Titre = $Titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->CreatedAt;
    }

    public function setCreatedAt(\DateTimeInterface $CreatedAt): self
    {
        $this->CreatedAt = $CreatedAt;

        return $this;
    }

    public function getHauteur(): ?int
    {
        return $this->Hauteur;
    }

    public function setHauteur(int $Hauteur): self
    {
        $this->Hauteur = $Hauteur;

        return $this;
    }

    public function getLargeur(): ?int
    {
        return $this->Largeur;
    }

    public function setLargeur(int $Largeur): self
    {
        $this->Largeur = $Largeur;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->Image;
    }

    public function setImage(?string $Image): self
    {
        $this->Image = $Image;

        return $this;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
*/
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        //if (null !== $imageFile) {
           // // It is required that at least one field changes if you are using doctrine
          //  // otherwise the event listeners won't be called and the file is lost
          //  $this->updatedAt = new \DateTimeImmutable();
      //  }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function getTechnique(): ?Technique
    {
        return $this->Technique;
    }

    public function setTechnique(?Technique $Technique): self
    {
        $this->Technique = $Technique;

        return $this;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->Categorie;
    }

    public function setCategorie(?Categorie $Categorie): self
    {
        $this->Categorie = $Categorie;

        return $this;
    }

    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function createSlug()
    {

        $slugify = new Slugify();
        $this->slug = $slugify->slugify($this->Titre);


    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getRegisterAt(): ?\DateTimeImmutable
    {
        return $this->RegisterAt;
    }

    public function setRegisterAt(?\DateTimeImmutable $RegisterAt): self
    {
        $this->RegisterAt = $RegisterAt;

        return $this;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
            $comment->setRelation($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getRelation() === $this) {
                $comment->setRelation(null);
            }
        }

        return $this;
    }
}
