<?php

namespace App\Form;

use App\Entity\Categorie;
use App\Entity\Painting;
use App\Entity\Technique;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PaintingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('Titre', TextType::class, [
                'label' => 'Titre de l\'oeuvre',
            ])
            ->add('Description',TextareaType::class, [
                'label' => 'description de l\'oeuvre',
            ])
            //->add('CreatedAt')
            ->add('Hauteur', IntegerType::class  , [
                'label' => 'hauteur',
            ] )
           ->add('Largeur', IntegerType::class , [
               'label' => 'largeur'
            ])
           // ->add('Image')
            ->add('Technique', EntityType::class,[
               'class' => Technique::class,
               'choice_label' => 'name'
           ])
            ->add('Categorie',EntityType::class,[
                'class' => Categorie::class,
                'choice_label' => 'name'
            ])

            ->add('createdAt', DateType::class,[
                'label' => 'Date de creation',
                'widget' => 'single_text',
                'html5'=>'false',
                'input' => 'datetime',


            ])

            ->add('imageFile', VichImageType::class,[
                'label' =>'choisissez votre image',
                'required'=>false,

            ])

            ->add('submit', SubmitType::class, [
                'label' => 'Ajouter...'
            ]);


    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Painting::class,
        ]);
    }
}
