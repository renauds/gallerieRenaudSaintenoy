<?php

namespace App\DataFixtures;
use App\Entity\Comment;
use App\Entity\Painting;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker;
class CommentFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');
        $painting = $manager->getRepository(Painting::class)->findAll();
        for($i = 1; $i<=24; $i++){

            $comment = new Comment();
            $name = $faker->name();
           $comment->setName($name)
                    ->setComment($faker->paragraphs(1, true))
                    ->setRelation($painting[$faker->numberBetween(0,count($painting)-1)])
                    ->setCreatedAt(new \DateTimeImmutable(date_format($faker->dateTimeBetween('-30 days', 'now'),"Y/m/d H:i:s")))
                    ->setIsPublished($faker->boolean(90));
                 $manager->persist($comment);
        }

        $manager->flush();
    }
    public function getDependencies ()
    {
        return [
            CategorieFixtures::class, TechniqueFixtures::class, PaintingFixtures::class
        ];
    }
}
