<?php

namespace App\DataFixtures;
use App\Entity\Painting;
use App\Entity\Categorie;
use App\Entity\Technique;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker;
class PaintingFixtures extends Fixture implements DependentFixtureInterface

{
    public function load ( ObjectManager $manager ): void
    {
        $faker = Faker\Factory::create('fr_FR');
        $categorie = $manager->getRepository(Categorie::class)->findAll();
        $technique = $manager->getRepository(Technique::class)->findAll();
        $slugify = new Slugify();
        for($i = 1; $i<=24; $i++){
            $post = new Painting();
            $title = $faker->words($faker->numberBetween(3,5),true);
            $post->setTitre($title)
                ->setDescription($faker->paragraphs(3, true))
                ->setCreatedAt(new \DateTime(date_format($faker->dateTimeBetween('-30 days', 'now'),"Y/m/d H:i:s")))
                ->setImage($i.'.jpg')
                ->setHauteur($faker->numberBetween(20,60))
                ->setLargeur($faker->numberBetween(30,60))
                ->setCategorie($categorie[$faker->numberBetween(0,count($categorie)-1)])
                ->setTechnique($technique[$faker->numberBetween(0,count($technique)-1)])
                ->setRegisterAt(new \DateTimeImmutable("now"))
                ->setSlug($slugify->slugify($title));

            $manager->persist($post);
        }

        $manager->flush();
    }
        public function getDependencies ()
        {
            return [
                CategorieFixtures::class, TechniqueFixtures::class
            ];
        }


}