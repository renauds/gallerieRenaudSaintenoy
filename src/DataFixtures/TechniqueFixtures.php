<?php

namespace App\DataFixtures;

use App\Entity\Categorie;
use App\Entity\Technique;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TechniqueFixtures extends Fixture
{
    private array $technique = ['Aquarelle', 'Gouache','Huile','Laque', 'Dripping'];
    public function load(ObjectManager $manager): void
    {
        foreach ($this->technique as $technik ) {
            $tec = new Technique();
            $tec->setName($technik);
            $manager->persist($tec);
            // $product = new Product();
            // $manager->persist($product);
        }
        $manager->flush();
    }
}
