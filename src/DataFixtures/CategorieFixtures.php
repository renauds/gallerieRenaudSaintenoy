<?php

namespace App\DataFixtures;

use App\Entity\Categorie;
use App\Entity\Technique;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategorieFixtures extends Fixture

{ private array $categorie = ['Abstrait','Classisisme', 'Cubisme', 'Expressionisme','Pointillisme', 'Surealisme'];
    public function load(ObjectManager $manager): void
    {
        foreach ($this->categorie as $category ){
            $cat = new Categorie();
            $cat->setName($category);
            $manager->persist($cat);

        }
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
